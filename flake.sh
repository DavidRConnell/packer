#!/usr/bin/env bash

cat <<-EOF > flake.nix
{
  description = "My project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.\${system};
      in { devShell = import ./shell.nix { inherit pkgs; }; });
}
EOF

cat <<-EOF > shell.nix
{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
  ];
}
EOF

git add {flake,shell}.nix
git commit -m "init: flake"

echo "use_flake" >> .envrc
